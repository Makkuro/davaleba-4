<?php
include 'db.php';

// kategoriebis migheba
$query = "SELECT * FROM categories";
$result = $mysqli->query($query);

// shemowmeba tu aris monacemebi
$categories = [];
if ($result) {
    while ($row = $result->fetch_assoc()) {
        $categories[] = $row;
    }
}
?>
<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="style.css"> 
    <title>Categories</title>
</head>
<body>
    <h1>Categories</h1>
    <ul class="categories">
        <?php foreach ($categories as $category): ?>
            <li class="category"><a href="category.php?id=<?= $category['id'] ?>"><?= htmlspecialchars($category['name']) ?></a></li>
        <?php endforeach; ?>
    </ul>
    <div class="admin-button">
        <a href="admin.php">Go to Admin Panel</a>
    </div>
</body>
</html>
