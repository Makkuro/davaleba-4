<?php
require 'db.php';

// kategoriis damateba
if (isset($_POST['add_category'])) {
    $name = $_POST['category_name'];
    $stmt = $mysqli->prepare("INSERT INTO categories (name) VALUES (?)");
    $stmt->bind_param('s', $name); // 's' for string
    $stmt->execute();
}

// kategoriis washla
if (isset($_POST['delete_category'])) {
    $category_id = $_POST['category_id'];
    $stmt = $mysqli->prepare("DELETE FROM categories WHERE id = ?");
    $stmt->bind_param('i', $category_id); // 'i' for integer
    $stmt->execute();
}

// kategoriis redaqtireba
if (isset($_POST['edit_category'])) {
    $category_id = $_POST['category_id'];
    $new_name = $_POST['new_category_name'];
    $stmt = $mysqli->prepare("UPDATE categories SET name = ? WHERE id = ?");
    $stmt->bind_param('si', $new_name, $category_id); // 'si' for string and integer
    $stmt->execute();
}

// produqtis damateba
if (isset($_POST['add_product'])) {
    $name = $_POST['product_name'];
    $description = $_POST['product_description'];
    $price = $_POST['product_price'];
    $category_id = $_POST['category_id'];
    $stmt = $mysqli->prepare("INSERT INTO products (name, description, price, category_id) VALUES (?, ?, ?, ?)");
    $stmt->bind_param('ssdi', $name, $description, $price, $category_id); // 'ssdi' for string, string, decimal, integer
    $stmt->execute();
}

// produqtis washla
if (isset($_POST['delete_product'])) {
    $product_id = $_POST['product_id'];
    $stmt = $mysqli->prepare("DELETE FROM products WHERE id = ?");
    $stmt->bind_param('i', $product_id); // 'i' for integer
    $stmt->execute();
}

// produqtis redaqtireba
if (isset($_POST['edit_product'])) {
    $product_id = $_POST['product_id'];
    $new_name = $_POST['new_product_name'];
    $new_description = $_POST['new_product_description'];
    $new_price = $_POST['new_product_price'];
    $stmt = $mysqli->prepare("UPDATE products SET name = ?, description = ?, price = ? WHERE id = ?");
    $stmt->bind_param('ssdi', $new_name, $new_description, $new_price, $product_id); // 'ssdi' for string, string, decimal, integer
    $stmt->execute();
}

// kategorieebis migheba
$result = $mysqli->query("SELECT * FROM categories");
$categories = $result->fetch_all(MYSQLI_ASSOC);

// produqtebis migheba
$query = "SELECT products.*, categories.name AS category_name FROM products JOIN categories ON products.category_id = categories.id";
$result = $mysqli->query($query);
$products = $result->fetch_all(MYSQLI_ASSOC);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Admin-Panel</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <h1>Admin Panel</h1>

    <!-- kategoriebis martva -->
    <section class="admin-categories">
        <h2>Categories</h2>
        <form method="POST">
            <input type="text" name="category_name" placeholder="Category Name" required>
            <button type="submit" name="add_category">Add Category</button>
        </form>

        <ul>
            <?php foreach ($categories as $category): ?>
                <li>
                    <strong><?= htmlspecialchars($category['name']) ?></strong>
                    <form method="POST" style="display: inline;">
                        <input type="hidden" name="category_id" value="<?= $category['id'] ?>">
                        <input type="text" name="new_category_name" placeholder="New name">
                        <button type="submit" name="edit_category">Edit</button>
                        <button type="submit" name="delete_category" style="background-color: #dc3545;">Delete</button>
                    </form>
                </li>
            <?php endforeach; ?>
        </ul>
    </section>

    <!-- produqtebis martva -->
    <section class="admin-products">
        <h2>Products</h2>
        <form method="POST">
            <input type="text" name="product_name" placeholder="Product Name" required>
            <input type="text" name="product_description" placeholder="Product Description" required>
            <input type="number" step="0.01" name="product_price" placeholder="Product Price" required>
            <select name="category_id" required>
                <option value="">Select Category</option>
                <?php foreach ($categories as $category): ?>
                    <option value="<?= $category['id'] ?>"><?= htmlspecialchars($category['name']) ?></option>
                <?php endforeach; ?>
            </select>
            <button type="submit" name="add_product">Add Product</button>
        </form>

        <ul>
            <?php foreach ($products as $product): ?>
                <li>
                    <strong><?= htmlspecialchars($product['name']) ?></strong> (<?= htmlspecialchars($product['category_name']) ?>)
                    <p><?= htmlspecialchars($product['description']) ?></p>
                    <p><strong>Price:</strong> $<?= htmlspecialchars($product['price']) ?></p>
                    <form method="POST" style="display: inline;">
                        <input type="hidden" name="product_id" value="<?= $product['id'] ?>">
                        <input type="text" name="new_product_name" placeholder="New Name">
                        <input type="text" name="new_product_description" placeholder="New Description">
                        <input type="number" step="0.01" name="new_product_price" placeholder="New Price">
                        <button type="submit" name="edit_product">Edit</button>
                        <button type="submit" name="delete_product" style="background-color: #dc3545;">Delete</button>
                    </form>
                </li>
            <?php endforeach; ?>
        </ul>
    </section>
</body>
</html>
