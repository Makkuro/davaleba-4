<?php
include 'db.php';

// kategoriis idis migheba
$category_id = $_GET['id'] ?? 0;

// queris momzadeba rom daemtxvas kategoriebi da produqtebi
$query = "SELECT * FROM products WHERE category_id = ?";
$stmt = $mysqli->prepare($query);
$stmt->bind_param('i', $category_id); // integer bindi
$stmt->execute();

// queris rezultatis migheba
$result = $stmt->get_result();
$products = $result->fetch_all(MYSQLI_ASSOC);
?>

<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="style.css"> 
    <title>Products</title> 
</head>
<body>
    <h1>Products</h1> <!-- hederi -->
    <ul class="products">
        <?php foreach ($products as $product): ?>
            <li class="product">
                <h2><?= htmlspecialchars($product['name']) ?></h2> <!-- Product name -->
                <p><?= htmlspecialchars($product['description']) ?></p> <!-- Product description -->
                <p class="price">Price: $<?= htmlspecialchars($product['price']) ?></p> <!-- Product price -->
            </li>
        <?php endforeach; ?>
    </ul>
</body>
</html>
