This is a simple eCommerce website project built with PHP and MySQL.

Files included:
- db.php: Database connection file.
- index.php: Displays all categories.
- category.php: Displays products for a selected category.
- admin.php: Admin panel for managing categories.

Setup Instructions:
1. Import the provided SQL structure into your phpMyAdmin or MySQL database.
2. Update 'db.php' with your database credentials.
3. Place these files in your web server's root directory.
4. Access 'index.php' in your browser to view the project.

Enjoy!
