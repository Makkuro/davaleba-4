<?php
$host = 'localhost'; // Database hosti
$db = 'ecommerce';   // Database name
$user = 'root';      // Database useri 
$pass = '';          // Database useris paroli (leave empty if no password)

// daconecteba MySQL databasis MySQLi gamoyenebit
$mysqli = new mysqli($host, $user, $pass, $db);

// kavshiris shemowmeba
if ($mysqli->connect_error) {
    // tu ver dakavshirda mashin errori gamova
    die("Database connection error: " . $mysqli->connect_error);
}
?>
