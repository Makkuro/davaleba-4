<?php
    include "file_folder.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>File Management</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <div class="container">
        <div class="controls">
            <div class="row">
                <a href="index.php" class="button">Home</a>
            </div>
            <div class="row">
                <form action="" method="post" class="form-inline">
                    <input type="text" placeholder="Folder name" name="folder"> 
                    <button class="button">Create Folder</button>
                </form>
            </div>
            <div class="row">
                <form action="" method="post" class="form-inline">
                    <input type="text" placeholder="File name" name="file"> 
                    <button class="button">Create File</button>
                </form>
            </div>
            <div class="row">
                <form action="" method="post" enctype="multipart/form-data" class="form-inline">
                    <input type="file" name="uploaded_file"> 
                    <button name="up_file" class="button">Upload File</button>
                </form>
            </div>
        </div>

        <?php if (!empty($editableFile)) { ?>
            <div class="edit-section">
                <h2>Editing File: <?= htmlspecialchars($editableFile) ?></h2>
                <form action="" method="post">
                    <textarea name="content" rows="20" cols="80"><?= htmlspecialchars($content) ?></textarea><br>
                    <input type="hidden" name="edit_file" value="<?= htmlspecialchars($editableFile) ?>">
                    <button type="submit" class="button">Save Changes</button>
                </form>
            </div>
        <?php } ?>

        <div class="content">
            <table class="dataset">
                <tr>
                    <th>Name</th>
                    <th>Rename</th>
                    <th>Delete</th>
                    <th>Download</th>
                    <th>Edit</th>
                </tr>
                <?php
                    for ($i = 2; $i < count($scan); $i++) {
                ?>
                <tr>
                    <td class="<?= is_file("MyDrive/" . $scan[$i]) ? "file" : "folder" ?>"> 
                        <span> <?= $scan[$i] ?> </span>
                    </td>
                    <td>
                        <form action="" method="post" class="form-inline">
                            <input type="text" placeholder="New name" name="new_name">
                            <input type="hidden" name="old_name" value="<?= $scan[$i] ?>">
                            <button class="button">Rename</button>
                        </form>
                    </td>
                    <td>
                        <a href="?source=<?= $scan[$i] ?>" class="button button-delete">Delete</a>
                    </td>       
                    <td>
                        <a href="<?= "MyDrive/" . $scan[$i] ?>" download class="button">
                            <?= is_file("MyDrive/" . $scan[$i]) ? "Download" : "" ?>
                        </a>
                    </td>
                    <td>
                        <form action="" method="post">
                            <input type="hidden" name="edit_file" value="<?= $scan[$i] ?>">
                            <button type="submit" class="button">Edit</button>
                        </form>
                    </td>
                </tr>
                <?php } ?>
            </table>
        </div>
    </div>
</body>
</html>
