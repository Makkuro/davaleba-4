<div class="test">
<?php
    
    if (isset($_POST['folder'])) {
        $n_folder = $_POST['folder'];
        mkdir("MyDrive/" . $n_folder);
    }

    
    if (isset($_POST['file'])) {
        $n_file = $_POST['file'];
        fopen("MyDrive/" . $n_file . ".txt", "w");
    }

    
    if (isset($_POST['new_name'])) {
        $new_name = $_POST['new_name'];
        $old_name = $_POST['old_name'];
        if (!is_file("MyDrive/" . $old_name)) {
            if (!is_dir("MyDrive/" . $new_name)) {
                rename("MyDrive/" . $old_name, "MyDrive/" . $new_name);
            } else {
                echo "<script>alert('The folder exists.')</script>";
            }
        } else {
            if (!file_exists("MyDrive/" . $new_name)) {
                rename("MyDrive/" . $old_name, "MyDrive/" . $new_name . ".txt");
            } else {
                echo "<script>alert('The File exists.')</script>";
            }
        }
    }

    if (isset($_GET['source'])) {
        $source = "MyDrive/" . $_GET['source'];
        
        
        if (is_file($source)) {
            unlink($source); 
        } elseif (is_dir($source)) {
            rmdir($source); 
        }; 
    }

    
    if (isset($_POST["up_file"])) {
        $file = $_FILES["uploaded_file"];
        $new_source = "MyDrive/" . $file["name"];
        move_uploaded_file($file["tmp_name"], $new_source);
    }

    
    $scan = scandir("MyDrive");

    
    $directoryPath = 'MyDrive/';
    $content = '';
    $editableFile = '';

    
    if (isset($_POST['edit_file'])) {
        $editableFile = $_POST['edit_file'];
        $filePath = $directoryPath . $editableFile;
        
        if (file_exists($filePath) && is_file($filePath)) {
            $content = file_get_contents($filePath);
        }
    }

    
    if (isset($_POST['content']) && !empty($editableFile)) {
        $newContent = $_POST['content'];
        $filePath = $directoryPath . $editableFile;
        file_put_contents($filePath, $newContent);
        $content = $newContent;
        
    }
?>
</div>
